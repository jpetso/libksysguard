# translation of ksysguardlsofwidgets.po to Belarusian Latin
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Ihar Hrachyshka <ihar.hrachyshka@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: ksysguardlsofwidgets\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-09-06 00:20+0000\n"
"PO-Revision-Date: 2008-12-27 00:31+0200\n"
"Last-Translator: Ihar Hrachyshka <ihar.hrachyshka@gmail.com>\n"
"Language-Team: Belarusian Latin <i18n@mova.org>\n"
"Language: be@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || n%10>=5 && n%10<=9 || n"
"%100>=11 && n%100<=14 ? 2 : 3);\n"

#: lsof.cpp:22
#, kde-format
msgctxt "Short for File Descriptor"
msgid "FD"
msgstr "FD"

#. i18n: ectx: property (text), widget (KLsofWidget, klsofwidget)
#: lsof.cpp:22 LsofSearchWidget.ui:28
#, kde-format
msgid "Type"
msgstr "Typ"

#: lsof.cpp:22
#, kde-format
msgid "Object"
msgstr "Abjekt"

#: LsofSearchWidget.cpp:25
#, kde-format
msgid "Renice Process"
msgstr "Źmiani vartaść „nice”"

#. i18n: ectx: property (text), widget (KLsofWidget, klsofwidget)
#: LsofSearchWidget.ui:23
#, kde-format
msgid "Stream"
msgstr "Płyń"

#. i18n: ectx: property (text), widget (KLsofWidget, klsofwidget)
#: LsofSearchWidget.ui:33
#, kde-format
msgid "Filename"
msgstr "Nazva fajła"
